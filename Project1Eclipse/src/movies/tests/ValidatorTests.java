/**
 *@author Kevin Echenique 
 */

package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import movies.importer.Validator;

import org.junit.jupiter.api.Test;

class ValidatorTests {

	@Test
	void testValidator() {
		Validator files = new Validator("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter", 
				"C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\ForTestsOnly");
		
		ArrayList<String> processedLines = new ArrayList<String>();
		processedLines.add("Title	123 Runtime	Year	kaggle");
		processedLines.add("The Masked Saint	111 minutes	2016	kaggle");
		
		ArrayList<String> sentences = files.process(processedLines);
		assertEquals("title	123	Year	kaggle", sentences.get(0));
		assertEquals("the masked saint	111	2016	kaggle", sentences.get(1));
	}

}
