/**
 * @author Kevin Echenique
 */

package movies.tests;
import movies.importer.Movie;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MoviesTests {

	@Test
	void movieTest() {
		//Testing get methods
		Movie movies = new Movie("pitufos", "120", "1900", "imaginary");
		assertEquals("1900", movies.getReleaseYear());
		assertEquals("pitufos", movies.getMovieName());
		assertEquals("120", movies.getMovieRuntime());
		assertEquals("imaginary", movies.getSource());
	}

}
