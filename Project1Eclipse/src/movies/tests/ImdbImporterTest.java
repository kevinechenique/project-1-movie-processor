package movies.tests;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import movies.importer.ImdbImporter;

import org.junit.jupiter.api.Test;

class ImdbImporterTest {

	@Test
	void processTest() {
		ImdbImporter files = new ImdbImporter ("C:\\Users\\juanc\\Desktop\\Dawson\\3rd semester\\java", 
				"C:\\Users\\juanc\\Desktop\\Dawson\\3rd semester\\java\\ForTest");
ArrayList<String> originalLines = new ArrayList<String>();
originalLines.add("imdb_title_id	title	original_title	year	date_published	genre	duration	country	language	director	writer	production_company	actors	description	avg_vote	votes	budget	usa_gross_income	worlwide_gross_income	metascore	reviews_from_users	reviews_from_critics");
originalLines.add("tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"				7	7");

//we are setting a new ArrayList to be equal to the ArrayList produced by our process method
ArrayList<String> newLines = files.process(originalLines);

//Each section is separated by a tab
assertEquals("title	duration	year	Imdb", newLines.get(0));
assertEquals("The Story of the Kelly Gang	70	1906	Imdb", newLines.get(1));

}


}
