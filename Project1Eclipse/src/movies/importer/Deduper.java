/**
 * @author Kevin Echenique
 * @author Juan Corzo
 */

package movies.importer;
import java.util.ArrayList;

public class Deduper extends Processor{
	
	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);//To verify later since no constructor was specified in the exercise
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<Movie> newInput = new ArrayList<Movie>();
		for(int i=0; i<input.size(); i++) {
			String currentSentence = input.get(i);
			String[] currentWord = currentSentence.split("\\t");
			if(currentWord.length == 4) {
				Movie movieobj = new Movie(currentWord[0], currentWord[1], currentWord[2], currentWord[3]);
				if(newInput.contains(movieobj)) {
					int index = newInput.indexOf(movieobj);
					String src = newInput.get(index).getSource() + ";" + movieobj.getSource();
					Movie combinedMovies = new Movie(movieobj.getMovieName(), movieobj.getMovieRuntime(), movieobj.getMovieRuntime(), src);
					newInput.set(index, combinedMovies);
				}
				else {newInput.add(movieobj);}
			}
		}
		ArrayList<String> processedList = new ArrayList<String>();
		for(int y=0; y<newInput.size(); y++) {
			processedList.add(newInput.get(y).toString());
			
		}
		return processedList;
	}
}
