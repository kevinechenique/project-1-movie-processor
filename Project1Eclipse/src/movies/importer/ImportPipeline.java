/**
 * @author Kevin Echenique
 * @author Juan Corzo
 */

package movies.importer;

import java.io.IOException;

public class ImportPipeline {

	public static void main(String[] args) throws IOException{
		
		KaggleImporter kaggle = new KaggleImporter("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\kaggle",
"C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\ProcessedFiles");
		
		Validator kaggleValidate = new Validator("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\ProcessedFiles",
				"C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\NormalizerOutput");
		
		ImdbImporter imdb = new ImdbImporter("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\imdb",
				"C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\ProcessedFiles");
		
		ImdbValidator imdbValidate = new ImdbValidator("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\NormalizerOutput",
				"C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\ValidatorOutput");
		
		Deduper dedu = new Deduper("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\ValidatorOutput",
				"C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\DeduperOutput");
		
		/*String root = /*"C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project1\\Project_1_MovieImporter\\ProcessedFiles" "C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\";
		String imdbSourceDir = root + "ImdbSmallFile";
		String kaggleSourceDir = root + "KaggleSmallFile";
		String importerOutputDir = root + "ProcessedFiles";
		String normalOutputDir = root + "NormalizerOutput";
		String valiOutputDir = root + "ValidatorOutput";
		String deduperOutput = root + "DeduperOutput";
		
		Processor[] processor = new Processor[5];
		processor[0] = new ImdbImporter(imdbSourceDir, importerOutputDir);
		processor[1] = new KaggleImporter(kaggleSourceDir, importerOutputDir);
		processor[2] = new ImdbValidator(importerOutputDir, normalOutputDir);
		processor[3] = new Validator(normalOutputDir, valiOutputDir);
		processor[4] = new Deduper(valiOutputDir, deduperOutput);
		
		processAll(processor);*/
		
		Processor[] processor = new Processor[5];
		processor[0] = kaggle;
		processor[1] = imdb;
		processor[3] = imdbValidate;
		processor[2] = kaggleValidate;
		processor[4] = dedu;
		processAll(processor);

	}
	
	public static void processAll(Processor[] processor) throws IOException {
		for(Processor value : processor) {
			value.execute();
		}
	}

}
