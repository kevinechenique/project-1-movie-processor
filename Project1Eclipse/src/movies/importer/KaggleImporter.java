/**
 * @author Kevin Echenique
 */

package movies.importer;

import java.util.ArrayList;

public class KaggleImporter extends Processor{
	
	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	//Iterates through the ArrayList containing the original text file's lines and returns a new arrayList with only the information we want
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> newInput = new ArrayList<String>();//will store all information relevant to our toString() created in the Movie object
		
		for(int i=0; i< input.size(); i++) {
			String currentSentence = input.get(i);//Temporarily stores the value of input at index i and splits the sentence into bits
			String[] splitWord = currentSentence.split("\\t");
			/*Creates a string with the relevant format to the toString() method and saves it in an index of our new array
			 * In this case it takes the title(15), runtime(13) and date released of the movie(20) and appends them together using the
			 * concat() method of the String class; we also add the constant source field at the end.
			 */
			
			newInput.add(splitWord[15].concat("\t"+splitWord[13]).concat("\t"+splitWord[20].concat("\t"+"kaggle")));
		
		}
		
		return newInput;
		
	}
	
}
