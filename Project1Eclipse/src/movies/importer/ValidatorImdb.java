package movies.importer;

import java.util.ArrayList;

public class ValidatorImdb extends Processor{
	public ValidatorImdb(String sourceDir, String outputDir) {
		super(sourceDir,outputDir, false);
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> newInput= new ArrayList<String>();
		
		for(int i=0; i<input.size(); i++) {
			String current =input.get(i);
			String[] splitWord=current.split("\\t");
			
			for(int j=0; j<splitWord.length ; j++) {
				if(isNullOrEmpty(splitWord[j])) {
					break;
				}
				try{
					int num=Integer.parseInt(splitWord[1]);
					int num2=Integer.parseInt(splitWord[2]);
				}catch(NumberFormatException e){
					break;
				}
			 newInput.add(input.get(i));
			}
			
		}
		return newInput;
	}
	public static boolean isNullOrEmpty(String str){
	        if(str == null || str.isEmpty()) {
	            return true;
	        }
	        return false;
	}
}
