/**
 * @author Juan Corzo
 */

package movies.importer;
import java.util.ArrayList;

public class ImdbValidator extends Processor{
	
	public ImdbValidator(String sourceDir, String outputDir) {
		super(sourceDir,outputDir, false);
	}
	
	/*public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> newInput= new ArrayList<String>();
		
		for(int i=0; i<input.size(); i++) {
			String current =input.get(i);
			String[] splitWord=current.split("\\t");
			
			for(int j=0; j<splitWord.length ; j++) {
				if(isNullOrEmpty(splitWord[j])) {
					break;
				}
				try{
					int num=Integer.parseInt(splitWord[1]);
					int num2=Integer.parseInt(splitWord[2]);
				}catch(NumberFormatException e){
					break;
				}
			 newInput.add(input.get(i));
			}
			
		}
		return newInput;
	}*/
	
	public ArrayList<String> process (ArrayList<String> input){
		ArrayList<String> newInput = new ArrayList<String>();
		
		boolean continueNext = false;//Determines whether an index should be stored inside the new ArrayList
		for(int i=0; i < input.size(); i++) {
			String currentSentence = input.get(i);
			String[] currentWord = currentSentence.split("\\t");
			continueNext = false;
			if(currentWord.length == 4) {
			for(int j=0; j<currentWord.length; j++) {
				if(currentWord[j].equals("") || currentWord[j] == null) {
					continueNext = true;
				}
			}
		
			if(!continueNext) {//if continueNext boolean is false then we add the word to our ArrayLsit
			newInput.add(input.get(i));
			}
		}
			
	}
		return newInput;
		
	}
	
	/*public static boolean isNullOrEmpty(String str){
	        if(str == null || str.isEmpty()) {
	            return true;
	        }
	        return false;
	}*/

}
