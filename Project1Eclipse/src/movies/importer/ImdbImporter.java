/**
 * @author Juan Corzo
 */

package movies.importer;
import java.util.ArrayList;

public class ImdbImporter extends Processor{
	
	public ImdbImporter (String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> newInput=new ArrayList<String>();
		for (int i=0; i<input.size(); i++) {
			String currentSentence= input.get(i);
			String[] splitWord=currentSentence.split("\\t",-1);
			Movie arrayMovie= new Movie (splitWord[1], splitWord[6], splitWord[3], "Imdb");
			newInput.add(arrayMovie.toString());
		}
		return newInput;
	}

}
