/**
 *@author Kevin Echenique 
 */
package movies.importer;
import java.util.ArrayList;

public class Validator extends Processor{

	public Validator(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	
	//Transforms the array given by KaggleImporter process() method and set all titles to lower case letters and the runtime just with numeric values 
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> newInput = new ArrayList<String>();
		
		//Converting all titles to lower case letters
		for(int i=0; i<input.size(); i++) {
			String currentSentence = input.get(i);
			String[] splitWord = currentSentence.split("\\t");
			
			//Stores the runtime and splits the first word from the second one so we can have: (e.g., 123[0] and minutes[1])
			String[] runtimeSeparate = splitWord[1].split(" ");
			
			newInput.add((splitWord[0].toLowerCase()).concat("\t"+ runtimeSeparate[0]).concat("\t" + splitWord[2]).concat("\t" + splitWord[3]));
		}
			return newInput;
		
	}
	
}
