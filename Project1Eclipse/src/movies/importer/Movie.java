/**
 * @author Kevin Echenique
 * @author Juan Corzo
 */

package movies.importer;

public class Movie {
	
	private String movieName;
	private String movieRuntime;
	private String releaseYear;
	
	//Stores where the movie is coming from
	private String source;
	
	public Movie(String movieName, String movieRuntime, String releaseYear, String source) {
		this.movieName = movieName;
		this.movieRuntime = movieRuntime;
		this.releaseYear = releaseYear;
		this.source = source;
	}
	
	//GET METHODS
	public String getReleaseYear() {
		return this.releaseYear;
	}
	public String getMovieName() {
		return this.movieName;
	}
	public String getMovieRuntime() {
		return this.movieRuntime;
	}
	public String getSource() {
		return this.source;
	}
	
	//toString method
	public String toString() {
		return(this.movieName + '\t' + this.movieRuntime + '\t' + this.releaseYear + '\t' + this.source);
	}

}
